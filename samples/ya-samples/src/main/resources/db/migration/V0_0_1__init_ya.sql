CREATE TABLE customer (
  id         BIGINT NOT NULL PRIMARY KEY,
  first_name VARCHAR(255),
  last_name  VARCHAR(255),
  geo GEOGRAPHY(POINT)
);