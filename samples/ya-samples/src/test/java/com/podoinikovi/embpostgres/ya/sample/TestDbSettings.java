package com.podoinikovi.embpostgres.ya.sample;

import com.podoinikovi.embpostgres.common.EmbPostgresInstance;
import com.podoinikovi.embpostgres.ya.EmbeddedPostgresYaInstance;
import com.podoinikovi.embpostgres.ya.YaEmbPostgresSettings;

import java.nio.file.Paths;

public abstract class TestDbSettings {
    public static YaEmbPostgresSettings settings() {
        YaEmbPostgresSettings settings = new YaEmbPostgresSettings();
        settings.setUseCustomDist(true);
        settings.setTempDirectory(Paths.get(System.getProperty("java.io.tmpdir") + "postgres-emb-ya-temp"));
        return settings;
    }

    public static EmbPostgresInstance postgresInstance() {
        return new EmbeddedPostgresYaInstance();
    }
}
