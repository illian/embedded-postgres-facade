package com.podoinikovi.embpostgres.ya.sample;

import com.podoinikovi.embpostgres.common.EmbPostgresRules;
import com.podoinikovi.embpostgres.common.EmbPostgresSuiteRule;
import junit.framework.TestCase;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CustomerRepositoryTest01.class, CustomerRepositoryTest02.class})
public class TestSuite extends TestCase {
    @ClassRule
    public static EmbPostgresSuiteRule rule =
            EmbPostgresRules.suiteRule(TestDbSettings.postgresInstance(), TestDbSettings.settings());
}
