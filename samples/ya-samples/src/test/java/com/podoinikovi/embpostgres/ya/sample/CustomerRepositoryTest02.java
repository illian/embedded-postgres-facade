package com.podoinikovi.embpostgres.ya.sample;

import com.podoinikovi.embpostgres.common.EmbPostgresClassRule;
import com.podoinikovi.embpostgres.common.EmbPostgresRules;
import lombok.extern.slf4j.Slf4j;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTest02 {
    @ClassRule
    public static EmbPostgresClassRule rule =
            EmbPostgresRules.classRule(TestDbSettings.postgresInstance(), TestDbSettings.settings());

    @Autowired
    private CustomerRepository repository;

    @Test
    public void test01() throws Exception {
        assertEquals(5, repository.findAll().size());
    }

    @Test
    public void test02() throws Exception {
        assertEquals(5, repository.findAll().size());
    }
}