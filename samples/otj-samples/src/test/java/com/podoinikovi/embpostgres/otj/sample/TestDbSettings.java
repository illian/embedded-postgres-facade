package com.podoinikovi.embpostgres.otj.sample;

import com.podoinikovi.embpostgres.common.EmbPostgresInstance;
import com.podoinikovi.embpostgres.otj.EmbeddedPostgresOtjInstance;
import com.podoinikovi.embpostgres.otj.OjtEmbPostgresSettings;

public abstract class TestDbSettings {
    public static OjtEmbPostgresSettings settings() {
        return new OjtEmbPostgresSettings();
    }

    public static EmbPostgresInstance postgresInstance() {
        return new EmbeddedPostgresOtjInstance();
    }
}
