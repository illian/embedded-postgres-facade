package com.podoinikovi.embpostgres.otj.sample;

import com.vividsolutions.jts.geom.Geometry;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name = "customer")
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

    @Id
    private Long id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column(name = "GEO", columnDefinition = "GEOGRAPHY(POINT)")
    private Geometry geo;
}
