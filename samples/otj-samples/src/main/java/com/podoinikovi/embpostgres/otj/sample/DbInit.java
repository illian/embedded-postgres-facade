package com.podoinikovi.embpostgres.otj.sample;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DbInit {
    private final CustomerRepository customerRepository;

    @PostConstruct
    protected void init() {
        if (customerRepository.count() == 0) {
            customerRepository.save(new Customer(1L, "Jack", "Bauer", GeoGeometryFactory.getGeometry(1d, 1d)));
            customerRepository.save(new Customer(2L, "Chloe", "O'Brian", GeoGeometryFactory.getGeometry(2d, 2d)));
            customerRepository.save(new Customer(3L, "Kim", "Bauer", GeoGeometryFactory.getGeometry(3d, 3d)));
            customerRepository.save(new Customer(4L, "David", "Palmer", GeoGeometryFactory.getGeometry(4d, 4d)));
            customerRepository.save(new Customer(5L, "Michelle", "Dessler", GeoGeometryFactory.getGeometry(5d, 5d)));

            log.info("Data init completed");
        }
    }
}
