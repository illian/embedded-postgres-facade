package com.podoinikovi.embpostgres.otj.sample;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;

public class GeoGeometryFactory {
    public static GeometryFactory createGeometryFactory() {
        return new GeometryFactory(new PrecisionModel(), 4326);
    }

    public static Point getGeometry(double lat, double lon) {
        return createGeometryFactory().createPoint(new Coordinate(lon, lat));
    }
}
