package com.podoinikovi.embpostgres.common.sample;

import com.podoinikovi.embpostgres.common.AbstractEmbPostgresSettings;
import com.podoinikovi.embpostgres.common.EmbPostgresInstance;
import com.podoinikovi.embpostgres.common.empty.EmbPostgresEmptyInstance;
import com.podoinikovi.embpostgres.common.empty.EmptyEmbPostgresSettings;

public abstract class TestDbSettings {
    public static AbstractEmbPostgresSettings settings() {
        return EmptyEmbPostgresSettings.DEFAULT_INSTANCE;
    }

    public static EmbPostgresInstance postgresInstance() {
        return EmbPostgresEmptyInstance.DEFAULT_INSTANCE;
    }
}
