package com.podoinikovi.embpostgres.common.sample;

import com.podoinikovi.embpostgres.common.EmbPostgresClassRule;
import com.podoinikovi.embpostgres.common.EmbPostgresRules;
import org.junit.ClassRule;
import org.junit.Test;

public class Test02 {
    public static final String LOG_PREFIX = "test 02, method ";

    @ClassRule
    public static EmbPostgresClassRule rule =
            EmbPostgresRules.classRule(TestDbSettings.postgresInstance(), TestDbSettings.settings());

    @Test
    public void test01() throws Exception {
        System.out.println(LOG_PREFIX + "01");
    }

    @Test
    public void test02() throws Exception {
        System.out.println(LOG_PREFIX + "02");
    }

    @Test
    public void test03() throws Exception {
        System.out.println(LOG_PREFIX + "03");
    }
}
