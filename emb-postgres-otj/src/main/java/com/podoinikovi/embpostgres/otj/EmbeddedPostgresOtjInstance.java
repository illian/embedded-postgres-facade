package com.podoinikovi.embpostgres.otj;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import com.podoinikovi.embpostgres.common.AbstractEmbPostgresSettings;
import com.podoinikovi.embpostgres.common.EmbPostgresInstance;

import java.io.File;
import java.io.IOException;

import static java.lang.String.format;

public class EmbeddedPostgresOtjInstance implements EmbPostgresInstance {
    private AbstractEmbPostgresSettings settings;

    private EmbeddedPostgres embeddedPostgres;

    @Override
    public void start() throws IOException {
        EmbeddedPostgres.Builder builder = EmbeddedPostgres.builder()
                .setPort(settings.getPort())
                .setPgBinaryResolver((system, machineHardware) ->
                        EmbPostgresInstance.class.getResourceAsStream(
                                format("/postgresql-%s-%s.txz", system, machineHardware))
                )
                .setCleanDataDirectory(true);
        if (settings.getTempDirectory() != null) {
            builder = builder.setDataDirectory(new File(settings.getTempDirectory().toString()));
        }

        embeddedPostgres = builder.start();
    }

    @Override
    public void stop() throws IOException {
        embeddedPostgres.close();
    }

    @Override
    public void applySettings(AbstractEmbPostgresSettings settings) {
        this.settings = settings;
    }
}
