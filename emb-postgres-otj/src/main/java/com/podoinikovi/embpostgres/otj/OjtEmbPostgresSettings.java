package com.podoinikovi.embpostgres.otj;

import com.podoinikovi.embpostgres.common.AbstractEmbPostgresSettings;

public class OjtEmbPostgresSettings extends AbstractEmbPostgresSettings {
    public void setPort(int port) {
        this.port = port;
    }

    public void setUseCustomDist(boolean useCustomDist) {
        this.useCustomDist = useCustomDist;
    }
}
