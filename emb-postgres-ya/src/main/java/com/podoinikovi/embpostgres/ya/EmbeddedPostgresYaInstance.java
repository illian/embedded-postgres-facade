package com.podoinikovi.embpostgres.ya;

import com.google.common.base.Preconditions;
import com.google.common.base.Verify;
import com.podoinikovi.embpostgres.common.AbstractEmbPostgresSettings;
import com.podoinikovi.embpostgres.common.EmbPostgresInstance;
import de.flapdoodle.embed.process.config.IRuntimeConfig;
import de.flapdoodle.embed.process.config.store.IDownloadConfig;
import de.flapdoodle.embed.process.io.directories.FixedPath;
import de.flapdoodle.embed.process.store.ArtifactStoreBuilder;
import de.flapdoodle.embed.process.store.PostgresArtifactStoreBuilder;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tukaani.xz.XZInputStream;
import ru.yandex.qatools.embed.postgresql.Command;
import ru.yandex.qatools.embed.postgresql.EmbeddedPostgres;
import ru.yandex.qatools.embed.postgresql.config.PostgresDownloadConfigBuilder;
import ru.yandex.qatools.embed.postgresql.config.RuntimeConfigBuilder;
import ru.yandex.qatools.embed.postgresql.distribution.Version;

import java.io.*;
import java.net.URL;
import java.nio.channels.FileLock;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.String.format;

/**
 We need start http server, because this implementation first try to download dist.

 We can put zero-size ZIP-file in resource directory with correct name, because before start
 Postgres server we extract TXZ-archive in temp directory and configure runtimeConfig for
 use files for this directory. Our downloaded ZIP-file will never be used.

 Maybe we can override package resolver and not use zero-size files.
 But why will we download big file? We will not use it.
 */
public class EmbeddedPostgresYaInstance implements EmbPostgresInstance {
    private static final String DOWNLOAD_URL_TEMPLATE = "http://localhost:%d/dist/";
    private static final String DIST_RESOURCE_DIRECTORY = "dist";

    private static final Logger LOG = LoggerFactory.getLogger(EmbeddedPostgres.class);

    private EmbeddedPostgres postgres;
    private AbstractEmbPostgresSettings settings;
    private Server server;

    @Override
    public void start() throws IOException {
        postgres = new EmbeddedPostgres(Version.Main.V9_6);
        Command cmd = Command.Postgres;

        ArtifactStoreBuilder artifactStoreBuilder = new PostgresArtifactStoreBuilder().defaults(cmd);

        if (settings.isUseCustomDist()) {
            if (settings.getTempDirectory() == null) {
                throw new IllegalArgumentException("For use custom dist option, temp directory is required!");
            }

            try {

                int port = startHttpServer();
                IDownloadConfig downloadConfig = (new PostgresDownloadConfigBuilder())
                        .defaultsForCommand(cmd)
                        .downloadPath(String.format(DOWNLOAD_URL_TEMPLATE, port))
                        .build();

                File file = prepareBinaries((system, machineHardware) -> EmbPostgresInstance.class.getResourceAsStream(
                        format("/postgresql-%s-%s.txz", system, machineHardware)));

                FixedPath cachedDir = new FixedPath(file.toPath().toString());
                artifactStoreBuilder = artifactStoreBuilder.download(downloadConfig).tempDir(cachedDir);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        IRuntimeConfig runtimeConfig = (new RuntimeConfigBuilder()).defaults(cmd)
                .artifactStore(artifactStoreBuilder.build())
                .build();

        postgres.start(runtimeConfig, "localhost", settings.getPort(), settings.getDatabaseName(),
                settings.getUsername(), settings.getPassword(), Collections.emptyList()
        );
    }

    private int startHttpServer() throws Exception {
        ResourceHandler resHandler = new ResourceHandler();
        resHandler.setDirectoriesListed(true);
        URL resource = EmbeddedPostgresYaInstance.class.getClassLoader().getResource(DIST_RESOURCE_DIRECTORY);
        if (resource != null) {
            resHandler.setResourceBase(resource.toString());

            server = new Server(0);

            ContextHandler contextHandler = new ContextHandler("/dist");
            contextHandler.setHandler(resHandler);
            server.setHandler(contextHandler);

            server.start();

            return server.getConnectors()[0].getLocalPort();
        } else {
            throw new IllegalArgumentException("Dist resource not found!");
        }
    }

    @Override
    public void stop() {
        if (postgres != null) {
            postgres.stop();
        }
        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void applySettings(AbstractEmbPostgresSettings settings) {
        this.settings = settings;
    }

    private static void extractTxz(String tbzPath, String targetDir) throws IOException {
        try (InputStream in = Files.newInputStream(Paths.get(tbzPath));
             XZInputStream xzIn = new XZInputStream(in);
             TarArchiveInputStream tarIn = new TarArchiveInputStream(xzIn)
        ) {
            TarArchiveEntry entry;

            while ((entry = tarIn.getNextTarEntry()) != null) {
                final String individualFile = entry.getName();
                final File fsObject = new File(targetDir + "/" + individualFile);

                if (entry.isSymbolicLink() || entry.isLink()) {
                    Path target = FileSystems.getDefault().getPath(entry.getLinkName());
                    Files.createSymbolicLink(fsObject.toPath(), target);
                } else if (entry.isFile()) {
                    byte[] content = new byte[(int) entry.getSize()];
                    int read = tarIn.read(content, 0, content.length);
                    Verify.verify(read != -1, "could not read %s", individualFile);
                    mkdirs(fsObject.getParentFile());
                    try (OutputStream outputFile = new FileOutputStream(fsObject)) {
                        IOUtils.write(content, outputFile);
                    }
                } else if (entry.isDirectory()) {
                    mkdirs(fsObject);
                } else {
                    throw new UnsupportedOperationException(
                            String.format("Unsupported entry found: %s", individualFile)
                    );
                }

                if (individualFile.startsWith("bin/") || individualFile.startsWith("./bin/")) {
                    fsObject.setExecutable(true);
                }
            }
        }
    }

    private static final AtomicReference<File> BINARY_DIR = new AtomicReference<>();
    private static final Lock PREPARE_BINARIES_LOCK = new ReentrantLock();
    private static final String LOCK_FILE_NAME = "epg-lock";

    private File prepareBinaries(PgBinaryResolver pgBinaryResolver) {
        PREPARE_BINARIES_LOCK.lock();
        try {
            if (BINARY_DIR.get() != null) {
                return BINARY_DIR.get();
            }

            final String system = getOS();
            final String machineHardware = getArchitecture();

            LOG.info("Detected a {} {} system", system, machineHardware);
            File pgDir;
            File pgsql;
            File pgTbz;
            final InputStream pgBinary;
            try {
                pgTbz = File.createTempFile("pgpg", "pgpg");
                pgBinary = pgBinaryResolver.getPgBinary(system, machineHardware);
            } catch (final IOException e) {
                throw new ExceptionInInitializerError(e);
            }

            if (pgBinary == null) {
                throw new IllegalStateException("No Postgres binary found for " + system + " / " + machineHardware);
            }

            try (final DigestInputStream pgArchiveData = new DigestInputStream(
                    pgBinary, MessageDigest.getInstance("MD5"));
                 final FileOutputStream os = new FileOutputStream(pgTbz)) {
                IOUtils.copy(pgArchiveData, os);
                pgArchiveData.close();
                os.close();

                String pgDigest = Hex.encodeHexString(pgArchiveData.getMessageDigest().digest());

                pgDir = new File(getWorkingDirectory(), String.format("PG-%s", pgDigest));
                pgsql = new File(pgDir, "pgsql");

                mkdirs(pgsql);
                final File unpackLockFile = new File(pgsql, LOCK_FILE_NAME);
                final File pgDirExists = new File(pgsql, ".exists");

                if (!pgDirExists.exists()) {
                    try (final FileOutputStream lockStream = new FileOutputStream(unpackLockFile);
                         final FileLock unpackLock = lockStream.getChannel().tryLock()) {
                        if (unpackLock != null) {
                            try {
                                Preconditions.checkState(!pgDirExists.exists(), "unpack lock acquired but .exists file is present.");
                                LOG.info("Extracting Postgres...");
                                extractTxz(pgTbz.getPath(), pgsql.getPath());
                                Verify.verify(pgDirExists.createNewFile(), "couldn't make .exists file");
                            } catch (Exception e) {
                                LOG.error("while unpacking Postgres", e);
                            }
                        } else {
                            // the other guy is unpacking for us.
                            int maxAttempts = 60;
                            while (!pgDirExists.exists() && --maxAttempts > 0) {
                                Thread.sleep(1000L);
                            }
                            Verify.verify(pgDirExists.exists(), "Waited 60 seconds for postgres to be unpacked but it never finished!");
                        }
                    } finally {
                        if (unpackLockFile.exists()) {
                            Verify.verify(unpackLockFile.delete(), "could not remove lock file %s", unpackLockFile.getAbsolutePath());
                        }
                    }
                }
            } catch (final IOException | NoSuchAlgorithmException e) {
                throw new ExceptionInInitializerError(e);
            } catch (final InterruptedException ie) {
                Thread.currentThread().interrupt();
                throw new ExceptionInInitializerError(ie);
            } finally {
                Verify.verify(pgTbz.delete(), "could not delete %s", pgTbz);
            }
            BINARY_DIR.set(pgDir);
            LOG.info("Postgres binaries at {}", pgDir);
            return pgDir;
        } finally {
            PREPARE_BINARIES_LOCK.unlock();
        }
    }

    private static void mkdirs(File dir) {
        Verify.verify(dir.mkdirs() || (dir.isDirectory() && dir.exists()), // NOPMD
                "could not create %s", dir);
    }

    private static String getOS() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return "Windows";
        }
        if (SystemUtils.IS_OS_MAC_OSX) {
            return "Darwin";
        }
        if (SystemUtils.IS_OS_LINUX) {
            return "Linux";
        }
        throw new UnsupportedOperationException("Unknown OS " + SystemUtils.OS_NAME);
    }

    /**
     * Get the machine architecture string. The string is used in the appropriate postgres binary name.
     *
     * @return Current machine architecture string.
     */
    private static String getArchitecture() {
        return "amd64".equals(SystemUtils.OS_ARCH) ? "x86_64" : SystemUtils.OS_ARCH;
    }

    private File getWorkingDirectory() {
        return new File(settings.getTempDirectory().toString());
    }
}
