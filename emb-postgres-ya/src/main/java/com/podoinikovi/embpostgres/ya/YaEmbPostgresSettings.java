package com.podoinikovi.embpostgres.ya;

import com.podoinikovi.embpostgres.common.AbstractEmbPostgresSettings;

import java.nio.file.Path;

public class YaEmbPostgresSettings extends AbstractEmbPostgresSettings {
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setUseCustomDist(boolean useCustomDist) {
        this.useCustomDist = useCustomDist;
    }

    public void setTempDirectory(Path tempDirectory) {
        this.tempDirectory = tempDirectory;
    }
}
