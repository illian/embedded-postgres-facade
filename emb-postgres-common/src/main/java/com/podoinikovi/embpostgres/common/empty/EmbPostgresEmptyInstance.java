package com.podoinikovi.embpostgres.common.empty;

import com.podoinikovi.embpostgres.common.AbstractEmbPostgresSettings;
import com.podoinikovi.embpostgres.common.EmbPostgresInstance;

import java.io.IOException;

public class EmbPostgresEmptyInstance implements EmbPostgresInstance {
    public static final EmbPostgresEmptyInstance DEFAULT_INSTANCE = new EmbPostgresEmptyInstance();

    protected EmbPostgresEmptyInstance() {

    }

    private static final String LOG_PREFIX = "Empty Embedded Postgres Instance ";

    @Override
    public void start() throws IOException {
        System.out.println(LOG_PREFIX + "start");
    }

    @Override
    public void stop() throws IOException {
        System.out.println(LOG_PREFIX + "stop");
    }

    @Override
    public void applySettings(AbstractEmbPostgresSettings settings) {
        System.out.println(LOG_PREFIX + "apply settings, " + settings);
    }
}
