package com.podoinikovi.embpostgres.common.empty;

import com.podoinikovi.embpostgres.common.AbstractEmbPostgresSettings;

public class EmptyEmbPostgresSettings extends AbstractEmbPostgresSettings {
    public static final EmptyEmbPostgresSettings DEFAULT_INSTANCE = new EmptyEmbPostgresSettings();

    protected EmptyEmbPostgresSettings() {

    }
}
