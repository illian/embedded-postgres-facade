package com.podoinikovi.embpostgres.common;

import org.junit.rules.ExternalResource;
import org.junit.runner.Description;
import org.junit.runners.Suite;
import org.junit.runners.model.Statement;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Collection;

public class EmbPostgresSuiteRule extends ExternalResource {
    private final EmbPostgresInstance postgresInstance;

    protected EmbPostgresSuiteRule(EmbPostgresInstance postgresInstance, AbstractEmbPostgresSettings settings) {
        this.postgresInstance = postgresInstance;
        this.postgresInstance.applySettings(settings);
    }

    @Override
    public Statement apply(Statement base, Description description) {
        Collection<Annotation> annotations = description.getAnnotations();
        boolean isSuiteAnnotationFound = false;
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(Suite.SuiteClasses.class)) {
                isSuiteAnnotationFound = true;
                break;
            }
        }
        if (!isSuiteAnnotationFound) {
            throw new IllegalArgumentException(
                    "This rule should be apply to suite only! Use EmbPostgresClassRule instead.");
        }

        return super.apply(base, description);
    }

    @Override
    protected void before() throws Throwable {
        postgresInstance.start();
    }

    @Override
    protected void after() {
        try {
            postgresInstance.stop();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
