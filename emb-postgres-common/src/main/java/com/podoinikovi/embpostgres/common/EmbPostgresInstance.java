package com.podoinikovi.embpostgres.common;

import java.io.IOException;

public interface EmbPostgresInstance {
    void start() throws IOException;

    void stop() throws IOException;

    void applySettings(AbstractEmbPostgresSettings settings);
}
