package com.podoinikovi.embpostgres.common;

import java.nio.file.Path;

public abstract class AbstractEmbPostgresSettings {
    public static final String DEFAULT_DATABASE_NAME = "postgres";
    public static final String DEFAULT_USERNAME = DEFAULT_DATABASE_NAME;
    public static final String DEFAULT_PASSWORD = DEFAULT_DATABASE_NAME;
    public static final int DEFAULT_PORT = 55432;
    public static final boolean DEFAULT_USE_CUSTOM_DIST = false;
    public static final Path DEFAULT_TEMP_DIRECTORY = null;

    protected String databaseName = DEFAULT_DATABASE_NAME;
    protected String username = DEFAULT_USERNAME;
    protected String password = DEFAULT_PASSWORD;
    protected int port = DEFAULT_PORT;
    protected boolean useCustomDist = DEFAULT_USE_CUSTOM_DIST;
    protected Path tempDirectory = DEFAULT_TEMP_DIRECTORY;

    public String getDatabaseName() {
        return databaseName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getPort() {
        return port;
    }

    public boolean isUseCustomDist() {
        return useCustomDist;
    }

    public Path getTempDirectory() {
        return tempDirectory;
    }

    public String getConnectionUrl() {
        return String.format("jdbc:postgresql://%s:%s/%s?user=%s&password=%s",
                "localhost",
                getPort(),
                getDatabaseName(),
                getUsername(),
                getPassword()
        );
    }

    @Override
    public String toString() {
        return "AbstractEmbPostgresSettings{" +
                "databaseName='" + databaseName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", port=" + port +
                ", useCustomDist=" + useCustomDist +
                ", tempDirectory=" + tempDirectory +
                '}';
    }
}
