package com.podoinikovi.embpostgres.common;

import org.junit.rules.ExternalResource;
import org.junit.runner.Description;
import org.junit.runners.Suite;
import org.junit.runners.model.Statement;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Collection;

public class EmbPostgresClassRule extends ExternalResource {
    private final EmbPostgresInstance postgresInstance;
    private final boolean isNotInSuiteTest;

    protected EmbPostgresClassRule(EmbPostgresInstance postgresInstance,
                                   AbstractEmbPostgresSettings settings, boolean isNotInSuiteTest) {
        this.postgresInstance = postgresInstance;
        this.isNotInSuiteTest = isNotInSuiteTest;
        if (isNotInSuiteTest) {
            this.postgresInstance.applySettings(settings);
        }
    }

    @Override
    public Statement apply(Statement base, Description description) {
        Collection<Annotation> annotations = description.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(Suite.SuiteClasses.class)) {
                throw new IllegalArgumentException(
                        "This rule doesn't apply to suite! Use EmbPostgresSuiteRule instead.");
            }
        }

        return super.apply(base, description);
    }

    @Override
    protected void before() throws Throwable {
        if (isNotInSuiteTest) {
            postgresInstance.start();
        }
    }

    @Override
    protected void after() {
        if (isNotInSuiteTest) {
            try {
                postgresInstance.stop();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
