package com.podoinikovi.embpostgres.common;

public class EmbPostgresRules {
    private static volatile EmbPostgresSuiteRule testRuleSuit;
    private static volatile EmbPostgresClassRule testRuleClass;

    public static EmbPostgresClassRule classRule(EmbPostgresInstance postgresInstance, AbstractEmbPostgresSettings settings) {
        EmbPostgresClassRule localInstance = testRuleClass;
        if (localInstance == null) {
            synchronized (EmbPostgresClassRule.class) {
                localInstance = testRuleClass;
                if (localInstance == null) {
                    boolean isNotInSuiteTest = testRuleSuit == null;
                    testRuleClass = localInstance = new EmbPostgresClassRule(postgresInstance, settings, isNotInSuiteTest);
                }
            }
        }

        return localInstance;
    }

    public static EmbPostgresSuiteRule suiteRule(EmbPostgresInstance postgresInstance, AbstractEmbPostgresSettings settings) {
        EmbPostgresSuiteRule localInstance = testRuleSuit;
        if (localInstance == null) {
            synchronized (EmbPostgresSuiteRule.class) {
                localInstance = testRuleSuit;
                if (localInstance == null) {
                    testRuleSuit = localInstance = new EmbPostgresSuiteRule(postgresInstance, settings);
                }
            }
        }

        return localInstance;
    }
}
